
const express = require("express");
const router = express.Router();
// sirve para recibir req
router.use(express.urlencoded({ extended: true }));
require('dotenv').config();


const { Sequelize } = require('sequelize');


const user = process.env.MYSQL_USER;
const password = process.env.MYSQL_PASSWORD;
const host = process.env.MYSQL_HOST;
const port = process.env.MYSQL_PORT;
const dbName = process.env.MYSQL_DB_NAME;
const url = `mysql://${user}:${password}@${host}:${port}/${dbName}` 
// Option 1: Passing a connection URI
const sequelize = new Sequelize(url); // Example for mysql

const conexion = async () => {
try {
    await sequelize.authenticate();
    console.log( 'Connection has been established successfully.');
  } catch (error) {
    console.log( `Unable to connect to the database: ${error}`);
  }
};

router.get('/conexion', function (req, res) {
  conexion();
  console.log(url);
  res.json("llego")
//   conexion();
});
module.exports = router;