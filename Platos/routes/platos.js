const express = require("express");
const router = express.Router();
// sirve para recibir req
router.use(express.urlencoded({ extended: true }));

const platos = require('../funciones/CRUD');
const midPlatos = require('../middleware/platos');
//crea un nuevo plato 
router.post("/", function (req, res) {
    platos.createPlato(req.body)
        .then(() => res.json('Plato guardado'))
        .catch(err => res.json(err));
});
//Lista todos los platos 
router.get("/", (req,res) => {
    platos.listPlatos()
        .then(platos => res.json(platos))
        .catch(err => res.json(err));
});
router.get('/:id',midPlatos.existePlato, function (req, res) {
    platos.getPlatoById(req.params.id)
    .then(dishes => res.json(dishes))
    .catch(err => res.json(err));
  });
//Actualiza id 
router.put('/:id', midPlatos.existePlato, function (req, res) {
    platos.updatePlato(req.params.id, req.body)
    .then(plato => res.json(plato))
    .catch(err => res.json(err));
  });
//Delete id 
router.delete('/:id', midPlatos.existePlato, function (req, res) {
    platos.deletePlato(req.params.id, req.body)
    .then(plato => res.json(plato))
    .catch(err => res.json(err));
  });

module.exports = router;
