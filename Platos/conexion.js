require('dotenv').config();
const mongoose = require('mongoose');

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

const MONGODB_HOST = process.env.MONGODB_HOST;
const MONGODB_PORT = process.env.MONGODB_PORT;
const MONGODB_MOVIE_DB_NAME = process.env.MONGODB_MOVIE_DB_NAME;

const conectionString = `mongodb://${MONGODB_HOST}:${MONGODB_PORT }/${MONGODB_MOVIE_DB_NAME}`;
console.log(conectionString)
mongoose.connect(conectionString, options)        
    .then(() => console.log("Database connected!"))
    .catch(err => console.log(`Error: ${err}`));
module.exports = mongoose;