const { response } = require("express");
const Platos = require('../modelo/platos');

//funcion para crear un nuevo plato
const createPlato = async(platoNew) => {
    const newPlatos = new Platos(platoNew);
    const response = await newPlatos.save();
    return response;
}

// lista con todos los platos 
const listPlatos = async () => await Platos.find();

const getPlatoById = async (platoId) => await Platos.findById(platoId);

const existePlato = async (platoId) => await Platos.exists({ _id: platoId });

const updatePlato = async (platoId, plato) => await Platos.findByIdAndUpdate(platoId, plato);

const deletePlato = async (platoId) => await Platos.findByIdAndDelete(platoId);




module.exports = {
    createPlato,
    listPlatos,
    existePlato,
    updatePlato,
    deletePlato,
    getPlatoById,
}