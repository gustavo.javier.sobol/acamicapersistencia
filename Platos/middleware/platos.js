const platos = require('../funciones/CRUD');

const existePlato = (req, res, next) => {
    platos.existePlato(req.params.id)
    .then((result) => result ? next() : res.status(404).send('Dish not found'))
    .catch(() => res.status(404).json('Dish not found'));
  }

module.exports = {
    existePlato
}