const express = require("express");
const router = express.Router();
const fetch = require('node-fetch');
router.use(express.urlencoded({ extended: true }));

const apiId = '39e5b168d05dc95006306ee67f5ae008';

const url ='https://api.openweathermap.org/data/2.5';
let resultCiudades = [];
let ciudades = [
    { nombre: "Rio Grande"},
    { nombre: "Ushuaia"},
    { nombre: "Tolhuin"},
    { nombre: "Rio Gallegos"},
    { nombre: "Piedrabuena"},
    { nombre: "San Julian"},
    { nombre: "Calafate"},
    { nombre: "Comodoro"},
    { nombre: "Trelew"},
    { nombre: "Madryn"}
];
/* 
let devCiudades = () => {
    
    for (let index = 0; index < 3; index++) {
        let id = Math.random() * (9 - 0) + 0;
        id = parseInt(id,10);
        console.log(ciudades[id].nombre);
    }
}

devCiudades();

 */

/* const createImg = (json) => {
  const dogImg = document.createElement('img');
  dogImg.src = json.message;
  main.appendChild(dogImg);
}
 */


const tempCiudade = (API) => {

    fetch(API)
    .then((response) => {
        return response.json();
    })
    .then(json => {
        console.log(JSON.stringify(json))
        resultCiudades.push(JSON.stringify(json));
    })
    .catch(err => console.log(`falló: ${err.message}`))
}


const getDogImg = () => {
    resultCiudades = [];
    for (let index = 0; index < 3; index++) {
        let id = parseInt(Math.random() * (9 - 0) + 0, 10);
        let ciudadSelec = ciudades[id].nombre;
        console.log(ciudadSelec);
        const API = `${url}/weather?q=${ciudadSelec}&appid=${apiId}`;
         tempCiudade(API);
         
    }
    return resultCiudades;
}



router.get("/", function (req, res) {
    res.json(getDogImg());
  });
;

module.exports = router;