const express = require("express");

const router = express.Router();

let mi_promesa = new Promise((resolve, reject) => {
	const number = Math.floor(Math.random() * 4);
  if(number > 0){
    resolve((number % 2)? "es impar" : "es par");
  }else{
    reject("es cero")
  }
});

mi_promesa
	.then(number => console.log(number))
	.catch(error => console.error(error));

module.exports = router;