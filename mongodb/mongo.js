const express = require("express");
const router = express.Router();
// sirve para recibir req
router.use(express.urlencoded({ extended: true }));

let mongoose = require("./config/conexion");

const schema = new mongoose.Schema({ nombre: String, apellido: String, edad: Number });
const Usuarios = mongoose.model("Usuarios", schema);

router.get('/findall', function (req, res) {
  Usuarios.find(function (err, data) {
    if (err) {
      console.log(err);
    }
    else {
      res.send(data);
    }
  });
});

router.get("/add", function (req, res) {
  const yo = { nombre: 'Gustavo', apellido: 'Sobol', edad: 39 };
  let nuevo_usuario = new Usuarios(yo);
  console.log(`antes save`);
  nuevo_usuario.save();
  console.log(`despues save`);
  res.json({ add: 'add Usuarios' });
})


router.get("/", function (req, res) {
  console.log('entra1');
  let todosLosUsuarios = Usuarios.find().then((resultados) => {
    console.log('entra');
    return resultados;
  });
  res.json({ listaUsuarios: JSON.stringify(todosLosUsuarios) });
});


module.exports = router;