const db = require('../config/conexion');


const Customer = db.model('Customer', new db.Schema({ name: String }));

const session =  db.startSession();
session.startTransaction();

// This `create()` is part of the transaction because of the `session`
// option.
 Customer.create([{ name: 'Test' }], { session: session });

// Transactions execute in isolation, so unless you pass a `session`
// to `findOne()` you won't see the document until the transaction
// is committed.
let doc =  Customer.findOne({ name: 'Test' });
assert.ok(!doc);

// This `findOne()` will return the doc, because passing the `session`
// means this `findOne()` will run as part of the transaction.
doc =  Customer.findOne({ name: 'Test' }).session(session);
assert.ok(doc);

// Once the transaction is committed, the write operation becomes
// visible outside of the transaction.
 session.commitTransaction();
doc =  Customer.findOne({ name: 'Test' });
assert.ok(doc);

session.endSession();