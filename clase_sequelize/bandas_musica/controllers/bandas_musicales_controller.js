require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../config/db.config");
const bandaMusicalesModel = require('../models/bandas_musicales.model')(connection, Sequelize);

const createBandaMusical = async (req) => {
  const bandaMusicalDetails = await bandaMusicalesModel.build({
    nombre: req.body.nombre,
    integrantes: req.body.integrantes,
    fecha_inicio: req.body.fecha_inicio,
    fecha_separacion: req.body.fecha_separacion,
    pais: req.body.pais
  });

  const result = await bandaMusicalDetails.save();
  return result;
}

const listBandaMusical = async () => await bandaMusicalesModel.findAll();

const updateBandaMusical = async (req) => {
  const id_bm = parseInt(req.params.id);

    const result = await bandaMusicalesModel.update(
      {
        nombre: req.body.nombre,
        integrantes: req.body.integrantes,
        fecha_inicio: req.body.fecha_inicio,
        fecha_separacion: req.body.fecha_separacion,
        pais: req.body.pais
      },
      { where: { id: id_bm } }
    );
    return result;
}

const deleteBandaMusical = async (req) => {
  const id_bm = req.params.id;
  const result = await bandaMusicalesModel.destroy({
    where: { id: id_bm }
  });
  return result;
}

module.exports = {
  createBandaMusical,
  listBandaMusical,
  updateBandaMusical,
  deleteBandaMusical
}
