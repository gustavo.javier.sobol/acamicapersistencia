

const bandaMusicalesModel = (connection, Sequelize) => {
  const bandaMusical = connection.define('bandas_musicales', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      nombre: {
        type: Sequelize.STRING
      },
      integrantes: {
        type: Sequelize.INTEGER,
      },
      fecha_inicio: {
        type: Sequelize.DATE
      },
      fecha_separacion: {
        type: Sequelize.DATE
      },
      pais: {
        type: Sequelize.STRING
      }
  },
  {
    timestamps: false
  });
  return bandaMusical
}

module.exports = bandaMusicalesModel;