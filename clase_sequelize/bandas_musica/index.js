const express = require("express");
const app = express();
require("dotenv").config();
const PORT = process.env.NODE_PORT || 5000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const bandaMusicalesRoutes = require("./routes/bandasMusicales.route");

app.use('/api/v1/bandas', bandaMusicalesRoutes)


app.listen(PORT, () => {
  console.log(`Server is running on port  http://localhost:${PORT}`);

});
