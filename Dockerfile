FROM node:16.1.0-alpine3.13
ENV NODE_ENV=production

RUN apk add tzdata
RUN cp /usr/share/zoneinfo/America/Argentina/Ushuaia /etc/localtime
RUN echo "America/Argentina/Ushuaia" >  /etc/timezone

RUN mkdir /MyAPP
WORKDIR /MyAPP
COPY package*.json ./

RUN npm install

RUN npm install -g nodemon

# COPY . .

EXPOSE 4000

# CMD ["npm", "start"]