const express = require("express");
const app = express();
require('dotenv').config();


// configuraciones iniciales
const port = process.env.NODE_PORT;
const host = "localhost";
app.use(express.json());

// configuraciones de promesa
/* const promesa = require("./promesa/clase");

app.use("/promesa", promesa); */

// configuraciones de promesa
const platos = require("./Platos/routes/platos");
app.use("/", platos);
/* 
const mongodb =  require("./mongodb/mongo");
app.use("/mongo", mongodb); */
// configuraciones de promesa
const conexion = require("./sequelize/conexion");
app.use("/mysql", conexion);

app.listen(port, () => {
  console.log(`Servidor corriendo en http://${host}:${port}`);
});
